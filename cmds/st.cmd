require adorca
require adpluginkafka

epicsEnvSet("PREFIX", "YMIR-Det1:")
epicsEnvSet("PORT",   "ORCA")
epicsEnvSet("QSIZE",  "21")
epicsEnvSet("XSIZE",  "2048")
epicsEnvSet("YSIZE",  "2048")
epicsEnvSet("NCHANS", "2048")
# Number of Elements
epicsEnvSet("NELEMENTS", "4194304")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "64000000")

#EVR settings
epicsEnvSet("EVR", "YMIR-TS:Ctrl-EVR-03")
epicsEnvSet("EVR_EVENT", "B")

# iocshLoad("$(adorca_DIR)ADOrca.iocsh")

devOrcaConfig("$(PORT)", 0, 0, 0, 0, 10)

dbLoadRecords("$(adorca_DIR)db/orca.db","P=$(PREFIX),R=cam1:,EVR=$(EVR),E=$(EVR_EVENT),PORT=$(PORT),ADDR=0,TIMEOUT=1")

#asynSetTraceMask("$(PORT)",-1,0x10)
#asynSetTraceIOMask("$(PORT)",-1,0x2)


# =========================================================================================================
# Create a PVA arrays plugin
# NDPvaConfigure (const char *portName, int queueSize, int blockingCallbacks,
#                      const char *NDArrayPort, int NDArrayAddr, const char *pvName,
#                      size_t maxMemory, int priority, int stackSize)
NDPvaConfigure("PVA1", $(QSIZE), 0, "$(PORT)", 0, "$(PREFIX)Pva1:Image", 0, 0, 0)
dbLoadRecords("$(adcore_DIR)/db/NDPva.template",  "P=$(PREFIX),R=Pva1:, PORT=PVA1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT)")
#

# Create a standard arrays plugin, set it to get data from orca driver.
NDStdArraysConfigure("Image1", "$(QSIZE)", 0, "$(PORT)", 0, 0)
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int32,FTVL=LONG,NELEMENTS=$(NELEMENTS)")

# Create a kafka plugin, set it to get data from orca driver.
epicsEnvSet("KFK_TOPIC", "ymir_camera")

# Kafka plugin
#epicsEnvSet("KFK_CONFIG_FILE_PATH", "/etc/kafka/kafka.conf")

#KafkaPluginConfigure("KFK1", 3, 1, "$(PORT)", 0, -1, "$(KFK_TOPIC)", "hama_kfk1","$(KFK_CONFIG_FILE_PATH)")
#dbLoadRecords("$(adpluginkafka_DIR)db/adpluginkafka.db", "P=$(PREFIX), R=Kfk1:, PORT=KFK1, ADDR=0, TIMEOUT=1, NDARRAY_PORT=$(PORT)")

# startPVAServer
iocInit()

dbpf $(PREFIX)cam1:PoolUsedMem.SCAN 0
dbpf $(PREFIX)image1:EnableCallbacks 1

dbpf $(PREFIX)cam1:AcquireTime .05
dbpf $(PREFIX)cam1:AcquirePeriod .1
dbpf $(PREFIX)cam1:NumImages 10
dbpf $(PREFIX)cam1:ImageMode 1

#dbpf Orca:cam1:TriggerPolarity-S    # 2-positive, 1-negative

epicsThreadSleep(1.0)

dbpf $(PREFIX)cam1:PoolUsedMem.SCAN 0
dbpf $(PREFIX)image1:EnableCallbacks 1

epicsThreadSleep(1.0)
