#ifndef DRV_HAMA_H
#define DRV_HAMA_H

// EPICS includes
#include <epicsExport.h>
#include <iocsh.h>

#include "ADDriver.h"
#include "dcamapi4.h"

#define MAX_FEATURE_NAME_LEN 64

#define MAX_H_SENSOR_SIZE 2048
#define MAX_V_SENSOR_SIZE 2048

#define hFrameRateString "H_FRAMERATE"
#define hOrcaNameString "H_HAMA_NAME"              /* asynOctet        ro */
#define hVendorString "H_VENDOR"                   /* asynOctet        ro */
#define hModelString "H_MODEL"                     /* asynOctet        ro */
#define hCameraIDString "H_CAMERAID"               /* asynOctet        ro */
#define hBusString "H_BUS"                         /* asynOctet        ro */
#define hCameraVersionString "H_CAMERA_VERSION"    /* asynOctet        ro */
#define hDriverVersionString "H_DRIVER_VERSION"    /* asynOctet        ro */
#define hModuleVersionString "H_MODULE_VERSION"    /* asynOctet        ro */
#define hDcamApiVersionString "H_DCAM_API_VERSION" /* asynOctet        ro */

// Sensor mode and speed
#define hSensorModeString "H_SENSOR_MODE"             /* asynint32  rw */
#define hReadoutSpeedString "H_READOUT_SPEED"         /* asynInt32  rw */
#define hReadoutDirectionString "H_READOUT_DIRECTION" /* asynIekt32  rw */

// Trigger
#define hTriggerSourceString "H_TRIGGER_SOURCE" /* asynInt32  rw */
#define hTriggerModeString "H_TRIGGER_MODE"     /* asynInt32  rw */
#define hTriggerActiveString "H_TRIGGER_ACTIVE" /* asynInt32  rw */
#define hTriggerGlobalExposureString \
  "H_TRIGGER_GLOBAL_EXPOSURE"                         /* asynInt32  rw */
#define hTriggerPolarityString "H_TRIGGER_POLARITY"   /* asynInt32  rw */
#define hTriggerConnectorString "H_TRIGGER_CONNECTOR" /* asynInt32  rw */
#define hTriggerTimesString "H_TRIGGER_TIMES"         /* asynInt32  rw */
#define hTriggerDelayString "H_TRIGGER_DELAY"         /* asynFloat64 rw */
#define hInternalTriggerHandlingString \
  "H_INTERNAL_TRIGGER_HANDLING" /* asynInt32  rw */

// Sensor cooler
#define hSensorTemperatureString "H_SENSOR_TEMPERATURE"    /* asynFloat64 ro */
#define hSensorCoolerString "H_SENSOR_COOLER"              /* asynInt32  rw */
#define hSensorCoolerStatusString "H_SENSOR_COOLER_STATUS" /* asynInt32  ro */

// Binning and ROI
#define hBinningString "H_BINNING"              /* asynInt32  rw */
#define hSubarrayHPosString "H_SUBARRAY_HPOST"  /* asynInt32  rw */
#define hSubarrayHSizeString "H_SUBARRAY_HSIZE" /* asynInt32  rw */
#define hSubarrayVPosString "H_SUBARRAY_VPOS"   /* asynInt32  rw */
#define hSubarrayVSizeString "H_SUBARRAY_VSIZE" /* asynInt32  rw */
#define hSubarrayModeString "H_SUBARRAY_MODE"   /* asynInt32  rw */

// ALU
#define hDefectCorrectModeString "H_DEFECTCORRECT_MODE" /* asynInt32  rw */
#define hHotPixelCorrectLevelString \
  "H_HOT_PIXEL_CORRECT_LEVEL"                          /* asynInt32  rw */
#define hIntensityLutModeString "H_INTENSITY_LUT_MODE" /* asynInt32  rw */
#define hIntensityLutPageString "H_INTENSITY_LUT_PAGE" /* asynInt32  ro */
#define hExtractionModeString "H_EXTRACTION_MODE"      /* asynInt32  ro */

// output trigger
#define hNumberOfOutputTriggerConnectorString \
  "H_NUMBEROF_OUTPUTTRIGGERCONNECTOR" /* asynInt32  ro */
#define hOutputTriggerSource0String \
  "H_OUTPUT_TRIGGER_SOURCE0" /* asynInt32  rw */
#define hOutputTriggerSource1String \
  "H_OUTPUT_TRIGGER_SOURCE1" /* asynInt32  rw */
#define hOutputTriggerSource2String \
  "H_OUTPUT_TRIGGER_SOURCE2" /* asynInt32  rw */
#define hOutputTriggerPolarity0String \
  "H_OUTPUT_TRIGGER_POLARITY0" /* asynInt32  rw */
#define hOutputTriggerPolarity1String \
  "H_OUTPUT_TRIGGER_POLARITY1" /* asynInt32  rw */
#define hOutputTriggerPolarity2String \
  "H_OUTPUT_TRIGGER_POLARITY2" /* asynInt32  rw */
#define hOutputTriggerActive0String \
  "H_OUTPUT_TRIGGER_ACTIVE0" /* asynInt32  ro */
#define hOutputTriggerActive1String \
  "H_OUTPUT_TRIGGER_ACTIVE1" /* asynInt32  ro */
#define hOutputTriggerActive2String \
  "H_OUTPUT_TRIGGER_ACTIVE2" /* asynInt32  ro */
#define hOutputTriggerDelay0String \
  "H_OUTPUT_TRIGGER_DELAY0" /* asynFloat64  rw */
#define hOutputTriggerDelay1String \
  "H_OUTPUT_TRIGGER_DELAY1" /* asynFloat64  rw */
#define hOutputTriggerDelay2String \
  "H_OUTPUT_TRIGGER_DELAY2" /* asynFloat64  rw */
#define hOutputTriggerPeriod0String \
  "H_OUTPUT_TRIGGER_PERIOD0" /* asynFloat64  rw */
#define hOutputTriggerPeriod1String \
  "H_OUTPUT_TRIGGER_PERIOD1" /* asynFloat64  rw */
#define hOutputTriggerPeriod2String \
  "H_OUTPUT_TRIGGER_PERIOD2"                               /* asynFloat64  rw */
#define hOutputTriggerKind0String "H_OUTPUT_TRIGGER_KIND0" /* asynInt32  rw */
#define hOutputTriggerKind1String "H_OUTPUT_TRIGGER_KIND1" /* asynInt32  rw */
#define hOutputTriggerKind2String "H_OUTPUT_TRIGGER_KIND2" /* asynInt32  rw */
#define hOutputTriggerBaseSensor0String \
  "H_OUTPUT_TRIGGER_BASE_SENSOR0" /* asynInt32  rw */
#define hOutputTriggerBaseSensor1String \
  "H_OUTPUT_TRIGGER_BASE_SENSOR1" /* asynInt32  rw */
#define hOutputTriggerBaseSensor2String \
  "H_OUTPUT_TRIGGER_BASE_SENSOR2" /* asynInt32  rw */
#define hOutputTriggerPreHsyncCountString \
  "H_OUTPUT_TRIGGER_PRE_HSYNC_COUNT" /* asynInt32  rw */

// Master Puls
#define hMasterPulseModeString "H_MASTERPULSE_MODE" /* asynInt32  rw */
#define hMasterPulseTriggerSourceString \
  "H_MASTERPULSE_TRIGGER_SOURCE" /* asynInt32  rw */
#define hMasterPulseIntervalString \
  "H_MASTERPULSE_INTERVAL" /* asynFloat64  rw */
#define hMasterPulseBurstTimesString \
  "H_MASTERPULSE_BURST_TIMES" /* asynInt32  rw */

// Synchronious Timing
#define hTimingReadoutTimeString "H_TIMING_READOUT_TIME" /* asynFloat64  ro */
#define hTimingCyclicTriggerPeriodString \
  "H_TIMING_CYCLICTRIGGERPERIOD" /* asynFloat64  ro */
#define hTimingMinTriggerBlankingString \
  "H_TIMING_MIN_TRIGGER_BLANKING" /* asynFloat64  ro */
#define hTimingMinTriggerIntervalString \
  "H_TIMING_MIN_TRIGGER_INTERVAL" /* asynFloat64  ro */
#define hTimingGlobalExposureDelayString \
  "H_TIMING_GLOBAL_EXPOSURE_DELAY"                /* asynFloat64  ro */
#define hTimingExposureString "H_TIMING_EXPOSURE" /* asynInt32  ro */
#define hTimingInvalidExposurePeriodString \
  "H_TIMING_INVALID_EXPOSURE_PERIOD"                     /* asynFloat64  ro */
#define hInternalFrameRateString "H_INTERNAL_FRAME_RATE" /* asynFloat64  rw */
#define hInternalFrameIntervalString \
  "H_INTERNAL_FRAME_INTERVAL"                            /* asynFloat64  rw */
#define hInternalLineSpeedString "H_INTERNAL_LINE_SPEED" /* asynFloat64  rw */
#define hInternalLineIntervalString \
  "H_INTERNAL_LINE_INTERVAL" /* asynFloat64  rw */

// System information
#define hColorTypeString "H_COLOR_TYPE"          /* asynInt32  ro */
#define hBitPerChannelString "H_BIT_PER_CHANNEL" /* asynInt32  rw */
#define hImageRowBytesString "H_IMAGE_ROWBYTES"     /* asynInt32  ro */
#define hImageFrameBytesString "H_IMAGE_FRAMEBYTES" /* asynInt32  ro */
#define hImageTopOffsetBytesString                                     \
  "H_IMAGE_TOP_OFFSETBYTES"                           /* asynInt32  ro \
                                                       */
#define hImagePixelTypeString "H_IMAGE_PIXEL_TYPE"    /* asynInt32  rw */
#define hBufferRowbytesString "H_BUFFER_ROWBYTES"     /* asynInt32  ro */
#define hBufferFramebytesString "H_BUFFER_FRAMEBYTES" /* asynInt32  ro */
#define hBufferTopOffsetBytesString \
  "H_BUFFER_TOP_OFFSETBYTES"                         /* asynInt32  ro */
#define hBufferPixelTypeString "H_BUFFER_PIXEL_TYPE" /* asynInt32  ro */
#define hRecordFixedBytesPerFileString \
  "H_RECORD_FIXED_BYTES_PER_FILE" /* asynInt32  ro */
#define hRecordFixedBytesPerSessionString \
  "H_RECORD_FIXED_BYTES_PER_SESION" /* asynInt32  ro */
#define hRecordFixedBytesPerFrameString \
  "H_RECORD_FIXED_BYTES_PER_FRAME"          /* asynInt32  ro */
#define hSystemAliveString "H_SYSTEM_ALIVE" /* asynInt32  ro */
#define hConversionFactorCoeffString \
  "H_CONVERSIONFACTOR_COEFF" /* asynFloat64  ro */
#define hConversionFactorOffsetString \
  "H_CONVERSIONFACTOR_OFFSET"                  /* asynFloat64  ro */
#define hNumberOfViewString "H_NUMBER_OF_VIEW" /* asynInt32  ro */
#define hTimeStampProducerString "H_TIMESTAMP_PRODUCER"   /* asynInt32  ro */
#define hFrameStampProducerString "H_FRAMESTAMP_PRODUCER" /* asynInt32  ro */
#define evrTimeStampString "EVR_TIMESTAMP"
#define evrCountString "EVR_COUNTS"
#define evrCountSinceAcqStartString "EVR_TRIGGERS_SINCE_ACQ_START"
#define evrTriggerDroppedString "EVR_TRIGGER_DROPPED"
#define hAcqControlString "H_ACQ_CONTROL"

// For some reason these two addresses are not defined in the dcamapi4 neither
// in dcamprop.h
#define DETECTOR_PIXEL_NUM_HORZ 0x00420830
#define DETECTOR_PIXEL_NUM_VERT 0x00420840

class epicsShareClass Orca : public ADDriver {
 public:
  Orca(const char*, int, size_t, int, int, int);
  // ~Orca();
  /* override ADDriver methods */
  virtual asynStatus writeInt32(asynUser* pasynUser, epicsInt32 value);
  virtual asynStatus writeFloat64(asynUser* pasynUser, epicsFloat64 value);
  virtual void report(FILE* fp, int details);

  void createAsynParams();
  int readParameter(int propertyID, bool processPV = true);
  int readParameterStr(int paramIndex);

  epicsUInt32 mTimeStampSec;
  epicsUInt32 mTimeStampNsec;

  void imageTask();
  void temperatureTask();

 protected:
  int hFrameRate;
#define FIRST_HAMA_PARAM hFrameRate
  int hOrcaName;
  int hVendor;
  int hModel;
  int hCameraID;
  int hBus;
  int hCameraVersion;
  int hDriverVersion;
  int hModuleVersion;
  int hDcamApiVersion;
  // - sensor mode and speed
  int hSensorMode;
  int hReadoutSpeed;
  int hReadoutDirection;
  // - trigger
  int hTriggerSource;
  int hTriggerMode;
  int hTriggerActive;
  int hTriggerGlobalExposure;
  int hTriggerPolarity;
  int hTriggerConnector;
  int hTriggerTimes;
  int hTriggerDelay;
  int hInternalTriggerHandling;
  // - sensor cooler
  int hSensorTemperature;
  int hSensorCooler;
  int hSensorCoolerStatus;
  // - binning and roi
  int hBinning;
  int hSubarrayHPos;
  int hSubarrayHSize;
  int hSubarrayVPos;
  int hSubarrayVSize;
  int hSubarrayMode;
  // - feature
  int hExposureTime;
  // - alu
  int hDefectCorrectMode;
  int hHotPixelCorrectLevel;
  int hIntensityLutMode;
  int hIntensityLutPage;
  int hExtractionMode;
  // - output trigger
  int hNumberOfOutputTriggerConnector;
  int hOutputTriggerSource0;
  int hOutputTriggerSource1;
  int hOutputTriggerSource2;
  int hOutputTriggerPolarity0;
  int hOutputTriggerPolarity1;
  int hOutputTriggerPolarity2;
  int hOutputTriggerActive0;
  int hOutputTriggerActive1;
  int hOutputTriggerActive2;
  int hOutputTriggerDelay0;
  int hOutputTriggerDelay1;
  int hOutputTriggerDelay2;
  int hOutputTriggerPeriod0;
  int hOutputTriggerPeriod1;
  int hOutputTriggerPeriod2;
  int hOutputTriggerKind0;
  int hOutputTriggerKind1;
  int hOutputTriggerKind2;
  int hOutputTriggerBaseSensor0;
  int hOutputTriggerBaseSensor1;
  int hOutputTriggerBaseSensor2;
  int hOutputTriggerPreHsyncCount;
  // - master pulse
  int hMasterPulseMode;
  int hMasterPulseTriggerSource;
  int hMasterPulseInterval;
  int hMasterPulseBurstTimes;
  // - symchronous timing
  int hTimingReadoutTime;
  int hTimingCyclicTriggerPeriod;
  int hTimingMinTriggerBlanking;
  int hTimingMinTriggerInterval;
  int hTimingGlobalExposureDelay;
  int hTimingExposure;
  int hTimingInvalidExposurePeriod;
  int hInternalFrameRate;
  int hInternalFrameInterval;
  int hInternalLineSpeed;
  int hInternalLineInterval;
  // - system information
  int hColorType;
  int hBitPerChannel;
  int hImageRowbytes;
  int hImageFramebytes;
  int hImageTopOffsetBytes;
  int hImagePixelType;
  int hBufferRowbytes;
  int hBufferFramebytes;
  int hBufferTopOffsetBytes;
  int hBufferPixelType;
  int hRecordFixedBytesPerFile;
  int hRecordFixedBytesPerSession;
  int hRecordFixedBytesPerFrame;
  int hSystemAlive;
  int hConversionFactorCoeff;
  int hConversionFactorOffset;
  int hNumberOfView;
  int hImageDetectorPixelWidth;
  int hImageDetectorPixelHeight;
  int hImageDetectorPixelNumHorz;
  int hImageDetectorPixelNumVert;
  int hTimeStampProducer;
  int hFrameStampProducer;
  int evrCounts;
  int evrCountsSinceAcqStart;
  int evrTriggerDropped;
  int hAcqControl;
  int evrTimeStamp;
  //----
  //#define LAST_HAMA_PARAM evrTimeStamp

 private:
  HDCAM m_hdcam;
  DCAMAPI_INIT m_apiInit;
  DCAMDEV_OPEN m_devOpen;
  DCAMERR m_err;
  HDCAMWAIT hwait;

  int stopThread;

  epicsEventId startEvent_;

  // Helper functions
  asynStatus startAcquire(void);
  asynStatus stopAcquire(void);
  void getImageInformation(HDCAM hdcam, int32& pixeltype, int32& width,
                           int32& rowbytes, int32& height, int32& framebytes);
  asynStatus accessCapturedImage(HDCAM hdcam, int32 iFrame, void* buf,
                                 int32 rowbytes, int32 cx, int32 cy,
                                 epicsUInt32& ts_sec, epicsUInt32& ts_microsec);
  asynStatus imageTransferStatus(HDCAM hdcam,
                                 DCAMCAP_TRANSFERINFO& captransferinfo);

  // wrapper functions for dcamapi
  int allocateBuffers(unsigned int);
  int freeBuffers();

  // These function will be read only once
  int connectCamera();
  int initCamera();

  bool checkAndLogError(DCAMERR err, const char* msg, int idx, double val,
                        const char* functionName);

  int roundToNearestMultipleOfFour(int value);

  asynStatus setFeature(int featureIndex, double value);
};

#endif
