#include "orca.h"

#include <string.h>

#include "dcamprop.h"

using namespace std;

static const char* driverName = "drvOrca";

// Error message formatters
#define ERR(msg)                                                         \
  asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s::%s: %s\n", driverName, \
            functionName, msg)

#define ERR_ARGS(fmt, ...)                                                    \
  asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s::%s: " fmt "\n", driverName, \
            functionName, __VA_ARGS__);

// Flow message formatters
#define FLOW(msg)                                                       \
  asynPrint(pasynUserSelf, ASYN_TRACE_FLOW, "%s::%s: %s\n", driverName, \
            functionName, msg)

#define FLOW_ARGS(fmt, ...)                                                  \
  asynPrint(pasynUserSelf, ASYN_TRACE_FLOW, "%s::%s: " fmt "\n", driverName, \
            functionName, __VA_ARGS__);

static void c_imagetask(void* arg) {
  Orca* p = (Orca*)arg;
  p->imageTask();
}

static void c_temperaturetask(void* arg) {
  Orca* p = (Orca*)arg;
  p->temperatureTask();
}

Orca::Orca(const char* portName, int maxBuffers, size_t maxMemory, int priority,
           int stackSize, int maxFrames)
    : ADDriver(portName, 1, 0, maxBuffers, maxMemory, asynEnumMask,
               asynEnumMask,
               ASYN_CANBLOCK, /* ASYN_CANBLOCK=1 ASYN_MULTIDEVICE=0 */
               1,             /* autoConnect=1 */
               priority, stackSize),
      m_hdcam(NULL) {
  const char* functionName = "orcaDetector";
  stopThread = 0;

  startEvent_ = epicsEventCreate(epicsEventEmpty);
  if (!startEvent_) {
    ERR("epicsEventCreate failure for acquire start event");
    return;
  }

  createAsynParams();

  setIntegerParam(NDArrayCallbacks, 1);
  setStringParam(ADStatusMessage, "Idle");
  callParamCallbacks();

  connectCamera();
  initCamera();

  /* launch image read task */
  epicsThreadCreate("OrcaImageTask", epicsThreadPriorityMedium,
                    epicsThreadGetStackSize(epicsThreadStackMedium),
                    c_imagetask, this);

  /* launch temp read task */
  epicsThreadCreate("OrcaTemperatureTask", epicsThreadPriorityMedium,
                    epicsThreadGetStackSize(epicsThreadStackMedium),
                    c_temperaturetask, this);
}

void Orca::createAsynParams() {
  // General
  createParam(hFrameRateString, asynParamFloat64, &hFrameRate);
  createParam(hOrcaNameString, asynParamOctet, &hOrcaName);
  createParam(hVendorString, asynParamOctet, &hVendor);
  createParam(hModelString, asynParamOctet, &hModel);
  createParam(hCameraIDString, asynParamOctet, &hCameraID);
  createParam(hBusString, asynParamOctet, &hBus);
  createParam(hCameraVersionString, asynParamOctet, &hCameraVersion);
  createParam(hDriverVersionString, asynParamOctet, &hDriverVersion);
  createParam(hModuleVersionString, asynParamOctet, &hModuleVersion);
  createParam(hDcamApiVersionString, asynParamOctet, &hDcamApiVersion);

  // Sensor Mode and Speed
  createParam(hSensorModeString, asynParamInt32, &hSensorMode);
  createParam(hReadoutSpeedString, asynParamInt32, &hReadoutSpeed);
  createParam(hReadoutDirectionString, asynParamInt32, &hReadoutDirection);

  // Trigger
  createParam(hTriggerSourceString, asynParamInt32, &hTriggerSource);
  createParam(hTriggerModeString, asynParamInt32, &hTriggerMode);
  createParam(hTriggerActiveString, asynParamInt32, &hTriggerActive);
  createParam(hTriggerGlobalExposureString, asynParamInt32,
              &hTriggerGlobalExposure);
  createParam(hTriggerPolarityString, asynParamInt32, &hTriggerPolarity);
  createParam(hTriggerConnectorString, asynParamInt32, &hTriggerConnector);
  createParam(hTriggerTimesString, asynParamInt32, &hTriggerTimes);
  createParam(hTriggerDelayString, asynParamFloat64, &hTriggerDelay);
  createParam(hInternalTriggerHandlingString, asynParamInt32,
              &hInternalTriggerHandling);

  // Sensor cooler
  createParam(hSensorTemperatureString, asynParamFloat64, &hSensorTemperature);
  createParam(hSensorCoolerString, asynParamInt32, &hSensorCooler);
  createParam(hSensorCoolerStatusString, asynParamInt32, &hSensorCoolerStatus);

  // Binning and ROI
  createParam(hBinningString, asynParamInt32, &hBinning);
  createParam(hSubarrayHPosString, asynParamInt32, &hSubarrayHPos);
  createParam(hSubarrayHSizeString, asynParamInt32, &hSubarrayHSize);
  createParam(hSubarrayVPosString, asynParamInt32, &hSubarrayVPos);
  createParam(hSubarrayVSizeString, asynParamInt32, &hSubarrayVSize);
  createParam(hSubarrayModeString, asynParamInt32, &hSubarrayMode);

  // ALU
  createParam(hDefectCorrectModeString, asynParamInt32, &hDefectCorrectMode);
  createParam(hHotPixelCorrectLevelString, asynParamInt32,
              &hHotPixelCorrectLevel);
  createParam(hIntensityLutModeString, asynParamInt32, &hIntensityLutMode);
  createParam(hIntensityLutPageString, asynParamInt32, &hIntensityLutPage);
  createParam(hExtractionModeString, asynParamInt32, &hExtractionMode);

  // output trigger
  createParam(hNumberOfOutputTriggerConnectorString, asynParamInt32,
              &hNumberOfOutputTriggerConnector);
  createParam(hOutputTriggerSource0String, asynParamInt32,
              &hOutputTriggerSource0);
  createParam(hOutputTriggerSource1String, asynParamInt32,
              &hOutputTriggerSource1);
  createParam(hOutputTriggerSource2String, asynParamInt32,
              &hOutputTriggerSource2);
  createParam(hOutputTriggerPolarity0String, asynParamInt32,
              &hOutputTriggerPolarity0);
  createParam(hOutputTriggerPolarity1String, asynParamInt32,
              &hOutputTriggerPolarity1);
  createParam(hOutputTriggerPolarity2String, asynParamInt32,
              &hOutputTriggerPolarity2);
  createParam(hOutputTriggerActive0String, asynParamInt32,
              &hOutputTriggerActive0);
  createParam(hOutputTriggerActive1String, asynParamInt32,
              &hOutputTriggerActive1);
  createParam(hOutputTriggerActive2String, asynParamInt32,
              &hOutputTriggerActive2);
  createParam(hOutputTriggerDelay0String, asynParamFloat64,
              &hOutputTriggerDelay0);
  createParam(hOutputTriggerDelay1String, asynParamFloat64,
              &hOutputTriggerDelay1);
  createParam(hOutputTriggerDelay2String, asynParamFloat64,
              &hOutputTriggerDelay2);
  createParam(hOutputTriggerPeriod0String, asynParamFloat64,
              &hOutputTriggerPeriod0);
  createParam(hOutputTriggerPeriod1String, asynParamFloat64,
              &hOutputTriggerPeriod1);
  createParam(hOutputTriggerPeriod2String, asynParamFloat64,
              &hOutputTriggerPeriod2);
  createParam(hOutputTriggerKind0String, asynParamInt32, &hOutputTriggerKind0);
  createParam(hOutputTriggerKind1String, asynParamInt32, &hOutputTriggerKind1);
  createParam(hOutputTriggerKind2String, asynParamInt32, &hOutputTriggerKind2);
  createParam(hOutputTriggerBaseSensor0String, asynParamInt32,
              &hOutputTriggerBaseSensor0);
  createParam(hOutputTriggerBaseSensor1String, asynParamInt32,
              &hOutputTriggerBaseSensor1);
  createParam(hOutputTriggerBaseSensor2String, asynParamInt32,
              &hOutputTriggerBaseSensor2);
  createParam(hOutputTriggerPreHsyncCountString, asynParamInt32,
              &hOutputTriggerPreHsyncCount);

  // Master Puls
  createParam(hMasterPulseModeString, asynParamInt32, &hMasterPulseMode);
  createParam(hMasterPulseTriggerSourceString, asynParamInt32,
              &hMasterPulseTriggerSource);
  createParam(hMasterPulseIntervalString, asynParamFloat64,
              &hMasterPulseInterval);
  createParam(hMasterPulseBurstTimesString, asynParamInt32,
              &hMasterPulseBurstTimes);

  // Synchronious Timing
  createParam(hTimingReadoutTimeString, asynParamFloat64, &hTimingReadoutTime);
  createParam(hTimingCyclicTriggerPeriodString, asynParamFloat64,
              &hTimingCyclicTriggerPeriod);
  createParam(hTimingMinTriggerBlankingString, asynParamFloat64,
              &hTimingMinTriggerBlanking);
  createParam(hTimingMinTriggerIntervalString, asynParamFloat64,
              &hTimingMinTriggerInterval);
  createParam(hTimingGlobalExposureDelayString, asynParamFloat64,
              &hTimingGlobalExposureDelay);
  createParam(hTimingExposureString, asynParamInt32, &hTimingExposure);
  createParam(hTimingInvalidExposurePeriodString, asynParamFloat64,
              &hTimingInvalidExposurePeriod);
  createParam(hInternalFrameRateString, asynParamFloat64, &hInternalFrameRate);
  createParam(hInternalFrameIntervalString, asynParamFloat64,
              &hInternalFrameInterval);
  createParam(hInternalLineSpeedString, asynParamFloat64, &hInternalLineSpeed);
  createParam(hInternalLineIntervalString, asynParamFloat64,
              &hInternalLineInterval);

  // System Information
  createParam(hColorTypeString, asynParamInt32, &hColorType);
  createParam(hBitPerChannelString, asynParamInt32, &hBitPerChannel);
  createParam(hImageRowBytesString, asynParamInt32, &hImageRowbytes);
  createParam(hImageFrameBytesString, asynParamInt32, &hImageFramebytes);
  createParam(hImageTopOffsetBytesString, asynParamInt32,
              &hImageTopOffsetBytes);
  createParam(hImagePixelTypeString, asynParamInt32, &hImagePixelType);
  createParam(hBufferRowbytesString, asynParamInt32, &hBufferRowbytes);
  createParam(hBufferFramebytesString, asynParamInt32, &hBufferFramebytes);
  createParam(hBufferTopOffsetBytesString, asynParamInt32,
              &hBufferTopOffsetBytes);
  createParam(hBufferPixelTypeString, asynParamInt32, &hBufferPixelType);
  createParam(hRecordFixedBytesPerFileString, asynParamInt32,
              &hRecordFixedBytesPerFile);
  createParam(hRecordFixedBytesPerSessionString, asynParamInt32,
              &hRecordFixedBytesPerSession);
  createParam(hRecordFixedBytesPerFrameString, asynParamInt32,
              &hRecordFixedBytesPerFrame);
  createParam(hSystemAliveString, asynParamInt32, &hSystemAlive);
  createParam(hConversionFactorCoeffString, asynParamFloat64,
              &hConversionFactorCoeff);
  createParam(hConversionFactorOffsetString, asynParamFloat64,
              &hConversionFactorOffset);
  createParam(hNumberOfViewString, asynParamInt32, &hNumberOfView);
  createParam(hTimeStampProducerString, asynParamInt32, &hTimeStampProducer);
  createParam(hFrameStampProducerString, asynParamInt32, &hFrameStampProducer);

  createParam(evrTimeStampString, asynParamOctet, &evrTimeStamp);
  createParam(evrCountString, asynParamInt32, &evrCounts);
  createParam(evrCountSinceAcqStartString, asynParamInt32,
              &evrCountsSinceAcqStart);
  createParam(evrTriggerDroppedString, asynParamInt32, &evrTriggerDropped);
  createParam(hAcqControlString, asynParamInt32, &hAcqControl);
}

int Orca::initCamera() {
  int err = 0;

  err |= readParameterStr(DCAM_IDSTR_VENDOR);
  err |= readParameterStr(DCAM_IDSTR_MODEL);
  err |= readParameterStr(DCAM_IDSTR_CAMERAID);
  err |= readParameterStr(DCAM_IDSTR_BUS);
  err |= readParameterStr(DCAM_IDSTR_CAMERAVERSION);
  err |= readParameterStr(DCAM_IDSTR_DRIVERVERSION);
  err |= readParameterStr(DCAM_IDSTR_MODULEVERSION);
  err |= readParameterStr(DCAM_IDSTR_DCAMAPIVERSION);

  // SENSOR MODE AND SPEED ------------------------
  err |= readParameter(DCAM_IDPROP_SENSORMODE);
  err |= readParameter(DCAM_IDPROP_READOUTSPEED);
  err |= readParameter(DCAM_IDPROP_READOUT_DIRECTION);

  // TRIGGER --------------------------------------
  err |= readParameter(DCAM_IDPROP_TRIGGERSOURCE);
  err |= readParameter(DCAM_IDPROP_TRIGGER_MODE);
  err |= readParameter(DCAM_IDPROP_TRIGGERACTIVE);
  err |= readParameter(DCAM_IDPROP_TRIGGER_GLOBALEXPOSURE);
  err |= readParameter(DCAM_IDPROP_TRIGGERPOLARITY);
  err |= readParameter(DCAM_IDPROP_TRIGGER_CONNECTOR);
  err |= readParameter(DCAM_IDPROP_TRIGGERTIMES);
  err |= readParameter(DCAM_IDPROP_TRIGGERDELAY);
  err |= readParameter(DCAM_IDPROP_INTERNALTRIGGER_HANDLING);

  // BINNING AND ROI ------------------------------
  err |= readParameter(DCAM_IDPROP_BINNING);
  err |= readParameter(DCAM_IDPROP_SUBARRAYHPOS);
  err |= readParameter(DCAM_IDPROP_SUBARRAYVPOS);
  err |= readParameter(DCAM_IDPROP_SUBARRAYHSIZE);
  err |= readParameter(DCAM_IDPROP_SUBARRAYVSIZE);
  err |= readParameter(DCAM_IDPROP_IMAGE_FRAMEBYTES);
  err |= readParameter(DCAM_IDPROP_IMAGE_PIXELTYPE);
  err |= readParameter(DCAM_IDPROP_IMAGE_WIDTH);
  err |= readParameter(DCAM_IDPROP_IMAGE_ROWBYTES);
  err |= readParameter(DCAM_IDPROP_IMAGE_HEIGHT);

  // ALU ---------------------------------------------------
  err |= readParameter(DCAM_IDPROP_DEFECTCORRECT_MODE);
  err |= readParameter(DCAM_IDPROP_HOTPIXELCORRECT_LEVEL);
  err |= readParameter(DCAM_IDPROP_INTENSITYLUT_MODE);
  err |= readParameter(DCAM_IDPROP_INTENSITYLUT_PAGE);
  err |= readParameter(DCAM_IDPROP_EXTRACTION_MODE);

  // OUTPUT TRIGGER ----------------------------------------
  err |= readParameter(DCAM_IDPROP_OUTPUTTRIGGER_ACTIVE);
  err |= readParameter(DCAM_IDPROP_NUMBEROF_OUTPUTTRIGGERCONNECTOR);

  // SYNCHRONOUS TIMING ------------------------------------
  err |= readParameter(DCAM_IDPROP_TIMING_READOUTTIME);
  err |= readParameter(DCAM_IDPROP_TIMING_CYCLICTRIGGERPERIOD);
  err |= readParameter(DCAM_IDPROP_TIMING_MINTRIGGERBLANKING);
  err |= readParameter(DCAM_IDPROP_TIMING_MINTRIGGERINTERVAL);
  err |= readParameter(DCAM_IDPROP_TIMING_GLOBALEXPOSUREDELAY);
  err |= readParameter(DCAM_IDPROP_TIMING_EXPOSURE);
  err |= readParameter(DCAM_IDPROP_TIMING_INVALIDEXPOSUREPERIOD);
  err |= readParameter(DCAM_IDPROP_INTERNALFRAMERATE);
  err |= readParameter(DCAM_IDPROP_INTERNAL_FRAMEINTERVAL);
  err |= readParameter(DCAM_IDPROP_INTERNALLINESPEED);
  err |= readParameter(DCAM_IDPROP_INTERNAL_LINEINTERVAL);

  // SYSTEM INFORMATION ------------------------------------
  err |= readParameter(DCAM_IDPROP_COLORTYPE);
  err |= readParameter(DCAM_IDPROP_BITSPERCHANNEL);
  err |= readParameter(DCAM_IDPROP_IMAGE_TOPOFFSETBYTES);
  err |= readParameter(DCAM_IDPROP_BUFFER_ROWBYTES);
  err |= readParameter(DCAM_IDPROP_BUFFER_FRAMEBYTES);
  err |= readParameter(DCAM_IDPROP_BUFFER_TOPOFFSETBYTES);
  err |= readParameter(DCAM_IDPROP_BUFFER_PIXELTYPE);
  err |= readParameter(DCAM_IDPROP_RECORDFIXEDBYTES_PERFILE);
  err |= readParameter(DCAM_IDPROP_RECORDFIXEDBYTES_PERSESSION);
  err |= readParameter(DCAM_IDPROP_RECORDFIXEDBYTES_PERFRAME);
  err |= readParameter(DCAM_IDPROP_SYSTEM_ALIVE);
  err |= readParameter(DCAM_IDPROP_CONVERSIONFACTOR_COEFF);
  err |= readParameter(DCAM_IDPROP_CONVERSIONFACTOR_OFFSET);
  err |= readParameter(DCAM_IDPROP_NUMBEROF_VIEW);
  err |= readParameter(DCAM_IDPROP_TIMESTAMP_PRODUCER);
  err |= readParameter(DCAM_IDPROP_FRAMESTAMP_PRODUCER);

  err |= readParameter(DETECTOR_PIXEL_NUM_HORZ);
  err |= readParameter(DETECTOR_PIXEL_NUM_VERT);

  return err;
}

int Orca::readParameterStr(int propertyID) {
  const char* functionName = "readParameterStr";
  asynStatus status = asynSuccess;

  char text[256];
  DCAMDEV_STRING param;
  memset(&param, 0, sizeof(param));
  param.size = sizeof(param);
  param.text = text;
  param.textbytes = sizeof(text);
  param.iString = propertyID;

  switch (propertyID) {
    case DCAM_IDSTR_VENDOR:
      dcamdev_getstring(m_hdcam, &param);
      status = setStringParam(hVendor, text);
      FLOW_ARGS("Vendor %s\n", text);
      break;
    case DCAM_IDSTR_MODEL:
      dcamdev_getstring(m_hdcam, &param);
      status = setStringParam(hModel, text);
      FLOW_ARGS("Model %s\n", text);
      break;
    case DCAM_IDSTR_CAMERAID:
      dcamdev_getstring(m_hdcam, &param);
      status = setStringParam(hCameraID, text);
      FLOW_ARGS("CameraID %s\n", text);
      break;
    case DCAM_IDSTR_BUS:
      dcamdev_getstring(m_hdcam, &param);
      status = setStringParam(hBus, text);
      FLOW_ARGS("Bus %s\n", text);
      break;
    case DCAM_IDSTR_CAMERAVERSION:
      dcamdev_getstring(m_hdcam, &param);
      status = setStringParam(hCameraVersion, text);
      FLOW_ARGS("CameraVersion %s\n", text);
      break;
    case DCAM_IDSTR_DRIVERVERSION:
      dcamdev_getstring(m_hdcam, &param);
      status = setStringParam(hDriverVersion, text);
      FLOW_ARGS("DriverVersion %s\n", text);
      break;
    case DCAM_IDSTR_MODULEVERSION:
      dcamdev_getstring(m_hdcam, &param);
      status = setStringParam(hModuleVersion, text);
      FLOW_ARGS("ModuleVersion %s\n", text);
      break;
    case DCAM_IDSTR_DCAMAPIVERSION:
      dcamdev_getstring(m_hdcam, &param);
      status = setStringParam(hDcamApiVersion, text);
      FLOW_ARGS("DCAMP Version %s\n", text);
      break;
    default:
      FLOW("NOT SUPPORTED\n");
      break;
  }

  callParamCallbacks();

  return status;
}

int Orca::readParameter(int propertyID, bool processPV) {
  const char* functionName = "readParameter";
  asynStatus status = asynSuccess;
  double dvalue = 0;

  switch (propertyID) {
    // - sensor mode and speed
    case DCAM_IDPROP_SENSORMODE:  //(RW1--)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SENSORMODE, &dvalue);
      status = setIntegerParam(hSensorMode, dvalue);
      break;
    case DCAM_IDPROP_READOUTSPEED:  //(RW1--)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_READOUTSPEED, &dvalue);
      status = setIntegerParam(hReadoutSpeed, dvalue);
      break;
    case DCAM_IDPROP_READOUT_DIRECTION:  // (RW1--)
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_READOUT_DIRECTION, &dvalue);
      status = setIntegerParam(hReadoutDirection, dvalue);
      break;
    // - trigger
    case DCAM_IDPROP_TRIGGERSOURCE:  // (RW123)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGERSOURCE, &dvalue);
      status = setIntegerParam(hTriggerSource, dvalue);
      break;
    case DCAM_IDPROP_TRIGGER_MODE:  // (RW123)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGER_MODE, &dvalue);
      status = setIntegerParam(hTriggerMode, dvalue);
      break;
    case DCAM_IDPROP_TRIGGERACTIVE:  //(RW123)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGERACTIVE, &dvalue);
      status = setIntegerParam(hTriggerActive, dvalue);
      break;
    case DCAM_IDPROP_TRIGGER_GLOBALEXPOSURE:  //(RW1--)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGER_GLOBALEXPOSURE,
                                &dvalue);
      status = setIntegerParam(hTriggerGlobalExposure, dvalue);
      break;
    case DCAM_IDPROP_TRIGGERPOLARITY:  //(RW123)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGERPOLARITY, &dvalue);
      status = setIntegerParam(hTriggerPolarity, dvalue);
      break;
    case DCAM_IDPROP_TRIGGER_CONNECTOR:  //(RW123)
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGER_CONNECTOR, &dvalue);
      status = setIntegerParam(hTriggerConnector, dvalue);
      break;
    case DCAM_IDPROP_TRIGGERTIMES:  //(RW123)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGERTIMES, &dvalue);
      status = setIntegerParam(hTriggerTimes, dvalue);
      break;
    case DCAM_IDPROP_TRIGGERDELAY:  //(RW123)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGERDELAY, &dvalue);
      status = setDoubleParam(hTriggerDelay, dvalue);
      break;
    case DCAM_IDPROP_INTERNALTRIGGER_HANDLING:  //(RW12-)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTERNALTRIGGER_HANDLING,
                                &dvalue);
      status = setIntegerParam(hInternalTriggerHandling, dvalue);
      break;
    // - sensor cooler
    case DCAM_IDPROP_SENSORCOOLER:  //(RW1--)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SENSORCOOLER, &dvalue);
      status = setIntegerParam(hSensorCooler, dvalue);
      break;
    case DCAM_IDPROP_SENSORCOOLERSTATUS:  //(R)
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SENSORCOOLERSTATUS, &dvalue);
      status = setIntegerParam(hSensorCoolerStatus, dvalue);
      break;

    // - binning and roi
    case DCAM_IDPROP_BINNING:  //(RW1--)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BINNING, &dvalue);
      status = setIntegerParam(hBinning, dvalue);
      break;

    case DCAM_IDPROP_SUBARRAYHSIZE:  //(RW1--)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SUBARRAYHSIZE, &dvalue);
      status = setIntegerParam(ADSizeX, dvalue);
      break;

    case DCAM_IDPROP_SUBARRAYVSIZE:  //(RW1--)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SUBARRAYVSIZE, &dvalue);
      status = setIntegerParam(ADSizeY, dvalue);
      break;

    case DCAM_IDPROP_SUBARRAYHPOS:  //(RW---)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SUBARRAYHPOS, &dvalue);
      status = setIntegerParam(ADMinX, dvalue);
      break;

    case DCAM_IDPROP_SUBARRAYVPOS:  //(RW1--)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SUBARRAYVPOS, &dvalue);
      status = setIntegerParam(ADMinY, dvalue);
      break;

    case DCAM_IDPROP_SUBARRAYMODE:  //(RW1--)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SUBARRAYMODE, &dvalue);
      status = setIntegerParam(hSubarrayMode, dvalue);
      break;

    case DCAM_IDPROP_EXPOSURETIME:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_EXPOSURETIME, &dvalue);
      status = setDoubleParam(ADAcquireTime, dvalue);
      break;

    // - alu
    case DCAM_IDPROP_DEFECTCORRECT_MODE:  //(RW123)
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_DEFECTCORRECT_MODE, &dvalue);
      status = setIntegerParam(hDefectCorrectMode, dvalue);
      break;
    case DCAM_IDPROP_HOTPIXELCORRECT_LEVEL:  //(RW123)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_HOTPIXELCORRECT_LEVEL,
                                &dvalue);
      status = setIntegerParam(hHotPixelCorrectLevel, dvalue);
      break;
    case DCAM_IDPROP_INTENSITYLUT_MODE:  //(RW123)
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTENSITYLUT_MODE, &dvalue);
      status = setIntegerParam(hIntensityLutMode, dvalue);
      break;
    case DCAM_IDPROP_INTENSITYLUT_PAGE:  //(RW1--)
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTENSITYLUT_PAGE, &dvalue);
      status = setIntegerParam(hIntensityLutPage, dvalue);
      break;
    case DCAM_IDPROP_EXTRACTION_MODE:  //(RW1--)
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_EXTRACTION_MODE, &dvalue);
      status = setIntegerParam(hExtractionMode, dvalue);
      break;

    // - output trigger
    case DCAM_IDPROP_NUMBEROF_OUTPUTTRIGGERCONNECTOR:
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_NUMBEROF_OUTPUTTRIGGERCONNECTOR, &dvalue);
      status = setIntegerParam(hNumberOfOutputTriggerConnector, dvalue);
      break;

    case DCAM_IDPROP_OUTPUTTRIGGER_SOURCE:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_SOURCE, &dvalue);
      status = setIntegerParam(hOutputTriggerSource0, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_SOURCE + 0x100, &dvalue);
      status = setIntegerParam(hOutputTriggerSource1, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_SOURCE + 0x200, &dvalue);
      status = setIntegerParam(hOutputTriggerSource2, dvalue);
      break;

    case DCAM_IDPROP_OUTPUTTRIGGER_POLARITY:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_POLARITY,
                                &dvalue);
      status = setIntegerParam(hOutputTriggerPolarity0, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_POLARITY + 0x100, &dvalue);
      status = setIntegerParam(hOutputTriggerPolarity1, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_POLARITY + 0x200, &dvalue);
      status = setIntegerParam(hOutputTriggerPolarity2, dvalue);
      break;

    case DCAM_IDPROP_OUTPUTTRIGGER_ACTIVE:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_ACTIVE, &dvalue);
      status = setIntegerParam(hOutputTriggerActive0, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_ACTIVE + 0x100, &dvalue);
      status = setIntegerParam(hOutputTriggerActive1, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_ACTIVE + 0x200, &dvalue);
      status = setIntegerParam(hOutputTriggerActive2, dvalue);
      break;

    case DCAM_IDPROP_OUTPUTTRIGGER_DELAY:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_DELAY, &dvalue);
      status = setIntegerParam(hOutputTriggerDelay0, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_DELAY + 0x100, &dvalue);
      status = setIntegerParam(hOutputTriggerDelay1, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_DELAY + 0x200, &dvalue);
      status = setIntegerParam(hOutputTriggerDelay2, dvalue);
      break;

    case DCAM_IDPROP_OUTPUTTRIGGER_PERIOD:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_PERIOD, &dvalue);
      status = setIntegerParam(hOutputTriggerPeriod0, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_PERIOD + 0x100, &dvalue);
      status = setIntegerParam(hOutputTriggerPeriod1, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_PERIOD + 0x200, &dvalue);
      status = setIntegerParam(hOutputTriggerPeriod2, dvalue);
      break;

    case DCAM_IDPROP_OUTPUTTRIGGER_KIND:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_KIND, &dvalue);
      status = setIntegerParam(hOutputTriggerKind0, dvalue);
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_KIND + 0x100,
                                &dvalue);
      status = setIntegerParam(hOutputTriggerKind1, dvalue);
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_KIND + 0x200,
                                &dvalue);
      status = setIntegerParam(hOutputTriggerKind2, dvalue);
      break;

    case DCAM_IDPROP_OUTPUTTRIGGER_BASESENSOR:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_BASESENSOR,
                                &dvalue);
      status = setIntegerParam(hOutputTriggerBaseSensor0, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_BASESENSOR + 0x100, &dvalue);
      status = setIntegerParam(hOutputTriggerBaseSensor1, dvalue);
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_BASESENSOR + 0x200, &dvalue);
      status = setIntegerParam(hOutputTriggerBaseSensor2, dvalue);
      break;

    case DCAM_IDPROP_OUTPUTTRIGGER_PREHSYNCCOUNT:
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_PREHSYNCCOUNT, &dvalue);
      status = setIntegerParam(hOutputTriggerPreHsyncCount, dvalue);
      break;

    // - master pulse
    case DCAM_IDPROP_MASTERPULSE_MODE:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_MASTERPULSE_MODE, &dvalue);
      status = setIntegerParam(hMasterPulseMode, dvalue);
      break;

    case DCAM_IDPROP_MASTERPULSE_TRIGGERSOURCE:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_MASTERPULSE_TRIGGERSOURCE,
                                &dvalue);
      status = setIntegerParam(hMasterPulseTriggerSource, dvalue);
      break;

    case DCAM_IDPROP_MASTERPULSE_INTERVAL:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_MASTERPULSE_INTERVAL, &dvalue);
      status = setIntegerParam(hMasterPulseInterval, dvalue);
      break;

    case DCAM_IDPROP_MASTERPULSE_BURSTTIMES:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_MASTERPULSE_BURSTTIMES,
                                &dvalue);
      status = setIntegerParam(hMasterPulseBurstTimes, dvalue);
      break;

    // - synchronous timing
    case DCAM_IDPROP_TIMING_READOUTTIME:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_READOUTTIME, &dvalue);
      status = setDoubleParam(hTimingReadoutTime, dvalue);
      break;
    case DCAM_IDPROP_TIMING_CYCLICTRIGGERPERIOD:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_CYCLICTRIGGERPERIOD,
                                &dvalue);
      status = setDoubleParam(hTimingCyclicTriggerPeriod, dvalue);
      break;
    case DCAM_IDPROP_TIMING_MINTRIGGERBLANKING:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_MINTRIGGERBLANKING,
                                &dvalue);
      status = setDoubleParam(hTimingMinTriggerBlanking, dvalue);
      break;
    case DCAM_IDPROP_TIMING_MINTRIGGERINTERVAL:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_MINTRIGGERINTERVAL,
                                &dvalue);
      status = setDoubleParam(hTimingMinTriggerInterval, dvalue);
      break;
    case DCAM_IDPROP_TIMING_GLOBALEXPOSUREDELAY:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_GLOBALEXPOSUREDELAY,
                                &dvalue);
      status = setDoubleParam(hTimingGlobalExposureDelay, dvalue);
      break;
    case DCAM_IDPROP_TIMING_EXPOSURE:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_EXPOSURE, &dvalue);
      status = setIntegerParam(hTimingExposure, dvalue);
      break;
    case DCAM_IDPROP_TIMING_INVALIDEXPOSUREPERIOD:
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_TIMING_INVALIDEXPOSUREPERIOD, &dvalue);
      status = setDoubleParam(hTimingInvalidExposurePeriod, dvalue);
      break;
    case DCAM_IDPROP_INTERNALFRAMERATE:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTERNALFRAMERATE, &dvalue);
      status = setDoubleParam(hInternalFrameRate, dvalue);
      break;
    case DCAM_IDPROP_INTERNAL_FRAMEINTERVAL:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTERNAL_FRAMEINTERVAL,
                                &dvalue);
      status = setDoubleParam(hInternalFrameInterval, dvalue);
      break;
    case DCAM_IDPROP_INTERNALLINESPEED:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTERNALLINESPEED, &dvalue);
      status = setDoubleParam(hInternalLineSpeed, dvalue);
      break;
    case DCAM_IDPROP_INTERNAL_LINEINTERVAL:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTERNAL_LINEINTERVAL,
                                &dvalue);
      status = setDoubleParam(hInternalLineInterval, dvalue);
      break;

    // - system information
    case DCAM_IDPROP_COLORTYPE:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_COLORTYPE, &dvalue);
      status = setIntegerParam(hColorType, dvalue);
      break;
    case DCAM_IDPROP_BITSPERCHANNEL:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BITSPERCHANNEL, &dvalue);
      status = setIntegerParam(hBitPerChannel, dvalue);
      break;
    case DCAM_IDPROP_IMAGE_ROWBYTES:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_ROWBYTES, &dvalue);
      status = setIntegerParam(hImageRowbytes, dvalue);
      break;
    case DCAM_IDPROP_IMAGE_FRAMEBYTES:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_FRAMEBYTES, &dvalue);
      status = setIntegerParam(hImageFramebytes, dvalue);
      break;
    case DCAM_IDPROP_IMAGE_TOPOFFSETBYTES:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_TOPOFFSETBYTES, &dvalue);
      status = setIntegerParam(hImageTopOffsetBytes, dvalue);
      break;
    case DCAM_IDPROP_IMAGE_PIXELTYPE:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_PIXELTYPE, &dvalue);
      status = setIntegerParam(hImagePixelType, dvalue);
      break;
    case DCAM_IDPROP_BUFFER_ROWBYTES:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BUFFER_ROWBYTES, &dvalue);
      status = setIntegerParam(hBufferRowbytes, dvalue);
      break;
    case DCAM_IDPROP_BUFFER_FRAMEBYTES:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BUFFER_FRAMEBYTES, &dvalue);
      status = setIntegerParam(hBufferFramebytes, dvalue);
      break;
    case DCAM_IDPROP_BUFFER_TOPOFFSETBYTES:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BUFFER_TOPOFFSETBYTES,
                                &dvalue);
      status = setIntegerParam(hBufferTopOffsetBytes, dvalue);
      break;
    case DCAM_IDPROP_BUFFER_PIXELTYPE:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BUFFER_PIXELTYPE, &dvalue);
      status = setIntegerParam(hBufferPixelType, dvalue);
      break;
    case DCAM_IDPROP_RECORDFIXEDBYTES_PERFILE:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_RECORDFIXEDBYTES_PERFILE,
                                &dvalue);
      status = setIntegerParam(hRecordFixedBytesPerFile, dvalue);
      break;
    case DCAM_IDPROP_RECORDFIXEDBYTES_PERSESSION:
      m_err = dcamprop_getvalue(
          m_hdcam, DCAM_IDPROP_RECORDFIXEDBYTES_PERSESSION, &dvalue);
      status = setIntegerParam(hRecordFixedBytesPerSession, dvalue);
      break;
    case DCAM_IDPROP_RECORDFIXEDBYTES_PERFRAME:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_RECORDFIXEDBYTES_PERFRAME,
                                &dvalue);
      status = setIntegerParam(hRecordFixedBytesPerFrame, dvalue);
      break;
    case DCAM_IDPROP_SYSTEM_ALIVE:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SYSTEM_ALIVE, &dvalue);
      status = setIntegerParam(hSystemAlive, dvalue);
      break;
    case DCAM_IDPROP_CONVERSIONFACTOR_COEFF:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_CONVERSIONFACTOR_COEFF,
                                &dvalue);
      status = setDoubleParam(hConversionFactorCoeff, dvalue);
      break;
    case DCAM_IDPROP_CONVERSIONFACTOR_OFFSET:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_CONVERSIONFACTOR_OFFSET,
                                &dvalue);
      status = setDoubleParam(hConversionFactorOffset, dvalue);
      break;
    case DCAM_IDPROP_NUMBEROF_VIEW:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_NUMBEROF_VIEW, &dvalue);
      status = setIntegerParam(hNumberOfView, dvalue);
      break;
    case DCAM_IDPROP_IMAGE_WIDTH:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_WIDTH, &dvalue);
      status = setIntegerParam(ADBinX, dvalue);
      break;
    case DCAM_IDPROP_IMAGE_HEIGHT:
      m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_HEIGHT, &dvalue);
      status = setIntegerParam(ADBinY, (int)dvalue);
      break;
    case DETECTOR_PIXEL_NUM_HORZ:
      m_err = dcamprop_getvalue(m_hdcam, DETECTOR_PIXEL_NUM_HORZ, &dvalue);
      status = setIntegerParam(ADMaxSizeX, dvalue);
      break;
    case DETECTOR_PIXEL_NUM_VERT:
      m_err = dcamprop_getvalue(m_hdcam, DETECTOR_PIXEL_NUM_VERT, &dvalue);
      status = setIntegerParam(ADMaxSizeY, dvalue);
      break;
    case DCAM_IDPROP_TIMESTAMP_PRODUCER:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMESTAMP_PRODUCER, &dvalue);
      status = setIntegerParam(hTimeStampProducer, dvalue);
      break;
    case DCAM_IDPROP_FRAMESTAMP_PRODUCER:
      m_err =
          dcamprop_getvalue(m_hdcam, DCAM_IDPROP_FRAMESTAMP_PRODUCER, &dvalue);
      status = setIntegerParam(hFrameStampProducer, dvalue);
      break;
    default:
      char text[256];
      dcamprop_getname(m_hdcam, propertyID, text, sizeof(text));
      FLOW_ARGS("Parameter not recognised, IDPROP:0x%08x, NAME:%s\n",
                propertyID, text);
      break;
  }

  if (failed(m_err)) {
    char text[256] = {'\0'};
    dcamprop_getname(m_hdcam, propertyID, text, sizeof(text));
    ERR_ARGS("DCAMERR: 0x%08X [%s: %s]", m_err, "dcamprop_getvalue", text);
  }

  if (processPV) status = callParamCallbacks();
  return status;
}

void Orca::imageTask() {
  const char* functionName = "imageTask";
  int status;
  unsigned char* image;
  int acquire;
  int count = 0;
  int callback;
  int imageMode = 0;
  int totalImages = 0;
  int initial_evr_counters = 0;
  int evr_counts_since_last_start = 0;
  int evr_counts_since_last_start_previous = 0;
  int evr_trigger_dropped = 0;
  int acqStatus;
  uint64_t prevAcquisitionCount = 0;
  DCAMCAP_TRANSFERINFO captransferinfo;
  epicsTimeStamp prevAcqTime, currentAcqTime;
  epicsTimeStamp prev_trigger_time, current_trigger_time;
  double elapsedTime;
  int evr_counts = 0;
  double exposure_time, readout_time;
  double maxAcqusitionTime, acqusitionRate;
  int triggerMode = DCAMPROP_TRIGGERSOURCE__INTERNAL;
  int triggerActive;

  lock();
  while (1) {
    getIntegerParam(ADAcquire, &acquire);
    if (!acquire) {
      unlock();
      status = epicsEventWait(startEvent_);
      lock();

      status = startAcquire();
      setStringParam(ADStatusMessage, "Acquiring");
      setIntegerParam(hAcqControl, 1);
      callParamCallbacks();

      epicsTimeGetCurrent(&prevAcqTime);
      if (status != asynSuccess) {
        epicsThreadSleep(.1);
      } else {
        getIntegerParam(ADNumImages, &totalImages);
        getIntegerParam(ADImageMode, &imageMode);
        setIntegerParam(ADStatus, ADStatusAcquire);
        acquire = 1;
        prevAcquisitionCount = 0;
        evr_trigger_dropped = 0;
        setIntegerParam(ADNumImagesCounter, 0);
        getIntegerParam(evrCounts, &initial_evr_counters);
        dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_READOUTTIME,
                          &readout_time);
        dcamprop_getvalue(m_hdcam, DCAM_IDPROP_EXPOSURETIME, &exposure_time);
      }
    }

    // get image transfer status.
    unlock();
    imageTransferStatus(m_hdcam, captransferinfo);
    lock();

    if (prevAcquisitionCount < (uint64_t)captransferinfo.nFrameCount) {
      FLOW_ARGS("Orca Frame Count: %d", captransferinfo.nFrameCount);
      prevAcquisitionCount = captransferinfo.nFrameCount;

      // get image information
      int32 pixeltype = 0, width = 0, rowbytes = 0, height = 0, framebytes = 0;
      getImageInformation(m_hdcam, pixeltype, width, rowbytes, height,
                          framebytes);

      if (pixeltype != DCAM_PIXELTYPE_MONO16) {
        FLOW("not implement");
        return;
      }

      setIntegerParam(NDArraySizeX, (int)width);
      setIntegerParam(NDArraySizeY, (int)height);
      /* Do callbacks so higher layers see any changes */
      callParamCallbacks();

      image = new unsigned char[width * 2 * height];
      memset(image, 0, width * 2 * height);

      getIntegerParam(NDArrayCounter, &count);
      count++;
      setIntegerParam(NDArrayCounter, count);

      epicsUInt32 ts_sec = 0, ts_microsec = 0;
      accessCapturedImage(m_hdcam, captransferinfo.nNewestFrameIndex, image,
                          width * 2, width, height, ts_sec, ts_microsec);

      double timestamp;

      getIntegerParam(hTriggerSource, &triggerMode);
      
      if (triggerMode == DCAMPROP_TRIGGERSOURCE__EXTERNAL) {
        getIntegerParam(evrCounts, &evr_counts);

        // Count events
        evr_counts_since_last_start = evr_counts - initial_evr_counters;
        evr_trigger_dropped =
            evr_counts_since_last_start - captransferinfo.nFrameCount;
        FLOW_ARGS("EVR event count: %d\n", evr_counts);
        FLOW_ARGS("evr_counts_since_last_start: %d\n",
                  evr_counts_since_last_start);

        // Update asyn parameters
        setIntegerParam(evrCountsSinceAcqStart, evr_counts_since_last_start);
        setIntegerParam(evrTriggerDropped, evr_trigger_dropped);
        char buf[256];
        getStringParam(evrTimeStamp, 256, buf);

        // remember parsed timestamp values
        sscanf(buf, "%u.%u", &mTimeStampSec, &mTimeStampNsec);
        timestamp = mTimeStampSec + mTimeStampNsec / 1.e9;
        FLOW_ARGS("mTimeStampSec: %u - mTimeStampNsec: %u\n", mTimeStampSec,
                  mTimeStampNsec);
        FLOW_ARGS("Timestamp from evr: %f\n", timestamp);


      } else {
        timestamp =
            (ts_sec + ts_microsec / 1.0e6) - (exposure_time + readout_time);
        FLOW_ARGS("Timestamp from camera: %f\n", timestamp);
    }

      FLOW_ARGS("readout_time: %f -  exposure_time: %f\n", readout_time,
                exposure_time);
      
      // calculate time since previous frame and update framerate.
      epicsTimeGetCurrent(&currentAcqTime);
      elapsedTime = epicsTimeDiffInSeconds(&currentAcqTime, &prevAcqTime);
      prevAcqTime = currentAcqTime;
      status = setDoubleParam(hFrameRate, (double)(1 / elapsedTime));
 
      getIntegerParam(NDArrayCallbacks, &callback);
      if (callback) {
        NDArray* pImage;
        size_t dims[2];
        int itemp = 0;

        getIntegerParam(NDArraySizeX, &itemp);
        dims[0] = itemp;
        getIntegerParam(NDArraySizeY, &itemp);
        dims[1] = itemp;
        pImage = pNDArrayPool->alloc(2, dims, NDUInt16, 0, 0);

        if (pImage) {
          pImage->uniqueId = count;
          pImage->timeStamp = timestamp;
          updateTimeStamp(&pImage->epicsTS);

          memcpy(pImage->pData, (epicsUInt16*)image, pImage->dataSize);

          getAttributes(pImage->pAttributeList);
          doCallbacksGenericPointer(pImage, NDArrayData, 0);
          pImage->release();
        }
      }

      delete[] image;
    }

    setIntegerParam(ADNumImagesCounter, captransferinfo.nFrameCount);

    // Check if evr count number has been updated and update parameters.
    getIntegerParam(evrCounts, &evr_counts);
    evr_counts_since_last_start = evr_counts - initial_evr_counters;
    if (evr_counts_since_last_start != evr_counts_since_last_start_previous) {
      setIntegerParam(evrCountsSinceAcqStart, evr_counts_since_last_start);
      epicsTimeGetCurrent(&prev_trigger_time);
      evr_counts_since_last_start_previous = evr_counts_since_last_start;
    }
  
    getIntegerParam(hTriggerActive, &triggerActive);

    if (triggerActive == DCAMPROP_TRIGGERACTIVE__SYNCREADOUT) {
      
    } else if (triggerActive == DCAMPROP_TRIGGERACTIVE__LEVEL) {
  
    } else  { 
    // Checks if the elapsed time since last frame is longer than the exposure,
    // stop waiting for image and put in timeout status.
      getDoubleParam(ADAcquirePeriod, &acqusitionRate);
      getIntegerParam(ADStatus, &acqStatus);
      maxAcqusitionTime = acqusitionRate * 2; // arbitrary time longer than the acqusiontime.
      epicsTimeGetCurrent(&currentAcqTime);
      if ((epicsTimeDiffInSeconds(&currentAcqTime, &prevAcqTime) >
            maxAcqusitionTime) &&
          acqStatus != ADStatusAborted) {
        FLOW_ARGS("maxAcqusitionTime %f\n", maxAcqusitionTime);
        setShutter(0);
        stopAcquire();
        setIntegerParam(ADAcquire, 0);
        setIntegerParam(hAcqControl, 0);
        setIntegerParam(ADStatus, ADStatusError);
        setStringParam(ADStatusMessage, "Acquisition timeout");
      }
    }

    // Check if the camera has failed a trigger from the evr. Updates the
    // amount dropped evr triggers.
    epicsTimeGetCurrent(&current_trigger_time);
    if (epicsTimeDiffInSeconds(&current_trigger_time, &prev_trigger_time) >
        (exposure_time + readout_time)) {
      if ((evr_counts_since_last_start) > captransferinfo.nFrameCount) {
        evr_trigger_dropped =
            evr_counts_since_last_start - captransferinfo.nFrameCount;
        setIntegerParam(evrTriggerDropped, evr_trigger_dropped);
      }
    }

    // Check if the acqusition is done (support single and multiple exposure)
    if ((imageMode == ADImageMultiple &&
         totalImages == captransferinfo.nFrameCount) ||
        (imageMode == ADImageSingle && captransferinfo.nFrameCount == 1)) {
      setShutter(0);
      stopAcquire();
      setIntegerParam(ADAcquire, 0);
      setIntegerParam(hAcqControl, 0);
      setIntegerParam(ADStatus, ADStatusIdle);
      setStringParam(ADStatusMessage, "Acquisition Done");
    }
    callParamCallbacks();
  }
}

void Orca::temperatureTask() {
  static const char* functionName = "tempTask";
  double dvalue = 0;

  while (!stopThread) {
    m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SENSORTEMPERATURE, &dvalue);
    if (failed(m_err)) {
      ERR_ARGS("DCAMERR: 0x%08X [%s: %s]", m_err, "dcamprop_getvalue",
               "DCAM_IDPROP_SENSORTEMPERATURE");
    } else
      setDoubleParam(hSensorTemperature, dvalue);

    m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SENSORCOOLERSTATUS, &dvalue);
    if (failed(m_err)) {
      ERR_ARGS("DCAMERR: 0x%08X [%s: %s]", m_err, "dcamprop_getvalue",
               "DCAM_IDPROP_SENSORCOOLERSTATUS");
    } else
      setIntegerParam(hSensorCoolerStatus, dvalue);

    callParamCallbacks();
    epicsThreadSleep(1.0);
  }
}

asynStatus Orca::writeInt32(asynUser* pasynUser, epicsInt32 value) {
  int status = asynSuccess;
  const char* functionName = "writeInt32";
  const char* paramName;

  int index = pasynUser->reason;
  int adstatus;
  double dvalue = static_cast<double>(value);

  getParamName(index, &paramName);

  if (index == ADAcquire) {
    getIntegerParam(ADStatus, &adstatus);
    if (value && adstatus != ADStatusAcquire) {
      double readoutTime, acquirePeriod, acquireTime;
      status |= getDoubleParam(hTimingReadoutTime, &readoutTime);
      status |= getDoubleParam(ADAcquireTime, &acquireTime);
      status |= getDoubleParam(ADAcquirePeriod, &acquirePeriod);

      if (acquirePeriod < (readoutTime + acquireTime)) {
        setStringParam(
            ADStatusMessage,
            "Acquire Period must be greater than ReadoutTime + AcquireTime");
        setIntegerParam(ADAcquire, 0);
        setIntegerParam(ADStatus, ADStatusAborted);
      } else {
        /* Send an event to wake up the acq task.*/
        setStringParam(ADStatusMessage, "");
        epicsEventSignal(this->startEvent_);
      }
    } else if (!value &&
               (adstatus == ADStatusAcquire || adstatus == ADStatusError ||
                adstatus == ADStatusWaiting)) {
      /* This was a command to stop acquisition */ //But not anymore?
      setIntegerParam(ADStatus, ADStatusAborted);
      setIntegerParam(hAcqControl, 0);
      setStringParam(ADStatusMessage, "Acquisition aborted by user");
      stopAcquire();
    }
  }
  // Handle ADMinX and ADMinY
  else if (index == ADMinX || index == ADMinY) {
    int size;
    double dFrameBytes = 0;

    if (index == ADMinX) {
      getIntegerParam(ADSizeX, &size);
    } else {
      getIntegerParam(ADSizeY, &size);
    }

    int maxSize = (index == ADMinX) ? MAX_H_SENSOR_SIZE : MAX_V_SENSOR_SIZE;
    if (size > (maxSize - value)) size = maxSize - value;

    double dSize = static_cast<double>(size);
    double dMin = static_cast<double>(value);
    int subArraySize = (index == ADMinX) ? DCAM_IDPROP_SUBARRAYHSIZE
                                         : DCAM_IDPROP_SUBARRAYVSIZE;
    int subArrayPos =
        (index == ADMinX) ? DCAM_IDPROP_SUBARRAYHPOS : DCAM_IDPROP_SUBARRAYVPOS;

    m_err = dcamprop_setgetvalue(m_hdcam, subArraySize, &dSize);
    m_err = dcamprop_setgetvalue(m_hdcam, subArrayPos, &dMin);
    dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_FRAMEBYTES, &dFrameBytes);

    if (index == ADMinX)
      setIntegerParam(ADSizeX, size);
    else
      setIntegerParam(ADSizeY, size);

    status |= setIntegerParam(hImageFramebytes, dFrameBytes);
    readParameter(DCAM_IDPROP_TIMING_READOUTTIME, false);
  }
  // Handle ADSizeX and ADSizeY
  else if (index == ADSizeX || index == ADSizeY) {
    int min;
    double dFrameBytes = 0;

    if (index == ADSizeX) {
      getIntegerParam(ADMinX, &min);
    } else {
      getIntegerParam(ADMinY, &min);
    }

    int maxSize = (index == ADSizeX) ? MAX_H_SENSOR_SIZE : MAX_V_SENSOR_SIZE;
    if (value > maxSize - min) value = maxSize - min;

    double dSize = roundToNearestMultipleOfFour(value);

    DCAMIDPROP subArraySize = (index == ADSizeX) ? DCAM_IDPROP_SUBARRAYHSIZE
                                                 : DCAM_IDPROP_SUBARRAYVSIZE;

    m_err = dcamprop_setgetvalue(m_hdcam, subArraySize, &dSize);
    dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_FRAMEBYTES, &dFrameBytes);

    if (index == ADSizeX) {
      setIntegerParam(ADSizeX, static_cast<int>(dSize));
    } else {
      setIntegerParam(ADSizeY, static_cast<int>(dSize));
    }

    status |= setIntegerParam(hImageFramebytes, dFrameBytes);
    readParameter(DCAM_IDPROP_TIMING_READOUTTIME, false);
  }

  //-- Output trigger
  else if (index >= hOutputTriggerSource0 && index <= hOutputTriggerSource2) {
    int offset = (index - hOutputTriggerSource0) / hOutputTriggerSource0;
    int prop = DCAM_IDPROP_OUTPUTTRIGGER_SOURCE + offset * 0x100;
    m_err = dcamprop_setgetvalue(m_hdcam, prop, &dvalue);
    checkAndLogError(m_err, "dcamprop_setgetvalue()", index, dvalue,
                     functionName);
  }

  //-- Sensor cooler
  else if (index == hSensorCooler) {
    m_err = dcamprop_setgetvalue(m_hdcam, DCAM_IDPROP_SENSORCOOLER, &dvalue);
    checkAndLogError(m_err, "dcamprop_setgetvalue()", index, dvalue,
                     functionName);
  }  

  //-- binning
  else if (index == hBinning) {
    double dFrameBytes = 0;
    m_err = dcamprop_setgetvalue(m_hdcam, DCAM_IDPROP_BINNING, &dvalue);
    checkAndLogError(m_err, "dcamprop_setgetvalue()", index, dvalue,
                     functionName);
    dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_FRAMEBYTES, &dFrameBytes);
    status = setIntegerParam(hImageFramebytes, dFrameBytes);
  }

  else if (index < FIRST_HAMA_PARAM) {
    status = ADDriver::writeInt32(pasynUser, value);
  }

  else {
    int prop = 0;
    if (index == hSensorMode) {
      prop = DCAM_IDPROP_SENSORMODE;
    } else if (index == hReadoutSpeed) {
      prop = DCAM_IDPROP_READOUTSPEED;
    } else if (index == hReadoutDirection) {
      prop = DCAM_IDPROP_READOUT_DIRECTION;
    } else if (index == hTriggerSource) {
      prop = DCAM_IDPROP_TRIGGERSOURCE;
    } else if (index == hTriggerMode) {
      prop = DCAM_IDPROP_TRIGGER_MODE;
    } else if (index == hTriggerActive) {
      prop = DCAM_IDPROP_TRIGGERACTIVE;
    } else if (index == hTriggerGlobalExposure) {
      prop = DCAM_IDPROP_TRIGGER_GLOBALEXPOSURE;
    } else if (index == hTriggerPolarity) {
      prop = DCAM_IDPROP_TRIGGERPOLARITY;
    } else if (index == hTriggerConnector) {
      prop = DCAM_IDPROP_TRIGGER_CONNECTOR;
    } else if (index == hTriggerTimes) {
      prop = DCAM_IDPROP_TRIGGERTIMES;
    } else if (index == hInternalTriggerHandling) {
      prop = DCAM_IDPROP_INTERNALTRIGGER_HANDLING;
    } else if (index == hSubarrayMode) {
      prop = DCAM_IDPROP_SUBARRAYMODE;
    } else if (index == hDefectCorrectMode) {
      prop = DCAM_IDPROP_DEFECTCORRECT_MODE;
    } else if (index == hHotPixelCorrectLevel) {
      prop = DCAM_IDPROP_HOTPIXELCORRECT_LEVEL;
    } else if (index == hIntensityLutMode) {
      prop = DCAM_IDPROP_INTENSITYLUT_MODE;
    } else if (index == hOutputTriggerPolarity0) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_POLARITY;
    } else if (index == hOutputTriggerPolarity1) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_POLARITY + 0x100;
    } else if (index == hOutputTriggerPolarity2) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_POLARITY + 0x200;
    } else if (index == hOutputTriggerKind0) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_KIND;
    } else if (index == hOutputTriggerKind1) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_KIND + 0x100;
    } else if (index == hOutputTriggerKind2) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_KIND + 0x200;
    } else if (index == hOutputTriggerBaseSensor0) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_BASESENSOR;
    } else if (index == hOutputTriggerBaseSensor1) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_BASESENSOR + 0x100;
    } else if (index == hOutputTriggerBaseSensor2) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_BASESENSOR + 0x200;
    } else if (index == hOutputTriggerPreHsyncCount) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_PREHSYNCCOUNT;
    } else if (index == hMasterPulseMode) {
      prop = DCAM_IDPROP_MASTERPULSE_MODE;
    } else if (index == hMasterPulseTriggerSource) {
      prop = DCAM_IDPROP_MASTERPULSE_TRIGGERSOURCE;
    } else if (index == hMasterPulseBurstTimes) {
      prop = DCAM_IDPROP_MASTERPULSE_BURSTTIMES;
    } else if (index == hBitPerChannel) {
      prop = DCAM_IDPROP_BITSPERCHANNEL;
    } else if (index == hImagePixelType) {
      prop = DCAM_IDPROP_IMAGE_PIXELTYPE;
    } else {
      return asynError;
    }

    m_err = dcamprop_setgetvalue(m_hdcam, prop, &dvalue);
    checkAndLogError(m_err, "dcamprop_setgetvalue()", prop, dvalue,
                     functionName);
  }

  if (status)
    asynPrint(pasynUser, ASYN_TRACE_ERROR,
              "%s:%s: error, status=%d function=%d, paramName=%s, value=%d\n",
              driverName, functionName, status, index, paramName, value);
  else {
    status = setIntegerParam(index, value);
    /* Do callbacks so higher layers see any changes */
    status = (asynStatus)callParamCallbacks();
    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, paramName=%s, value=%d\n", driverName,
              functionName, index, paramName, value);
  }

  return (asynStatus)status;
}

asynStatus Orca::writeFloat64(asynUser* pasynUser, epicsFloat64 value) {
  asynStatus status = asynSuccess;
  const char* functionName = "writeFloat64";
  const char* paramName;
  int index = pasynUser->reason;
  double dvalue = value;
  getParamName(index, &paramName);

  if (index == ADAcquireTime) {
    int trigger_mode = 0;
    double acquirePeriod = 0;
    status = getDoubleParam(ADAcquirePeriod, &acquirePeriod);
    status = getIntegerParam(hTriggerSource, &trigger_mode);

    if (trigger_mode == DCAMPROP_TRIGGERSOURCE__EXTERNAL) {
      if (value >= acquirePeriod)
        status = asynError;
      else {
        status = setFeature(DCAM_IDPROP_EXPOSURETIME, value);
      }
    }

    else {
      status = setFeature(DCAM_IDPROP_EXPOSURETIME, value);
      if (value >= acquirePeriod) {
        status = setFeature(DCAM_IDPROP_TRIGGERSOURCE,
                            DCAMPROP_TRIGGERSOURCE__INTERNAL);
        status =
            setIntegerParam(hTriggerSource, DCAMPROP_TRIGGERSOURCE__INTERNAL);
      }
    }

    readParameter(DCAM_IDPROP_TIMING_READOUTTIME, false);
    m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_EXPOSURETIME, &value);
  }

  else if (index == ADAcquirePeriod) {
    int trigger_mode = 0;
    getIntegerParam(hTriggerSource, &trigger_mode);

    m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_EXPOSURETIME, &dvalue);
    if (trigger_mode != DCAMPROP_TRIGGERSOURCE__EXTERNAL) {
      if (value <= dvalue) {
        status = setFeature(DCAM_IDPROP_TRIGGERSOURCE,
                            DCAMPROP_TRIGGERSOURCE__INTERNAL);
        status =
            setIntegerParam(hTriggerSource, DCAMPROP_TRIGGERSOURCE__INTERNAL);
      } else {
        status = setFeature(DCAM_IDPROP_TRIGGERSOURCE,
                            DCAMPROP_TRIGGERSOURCE__MASTERPULSE);
        status = setIntegerParam(hTriggerSource,
                                 DCAMPROP_TRIGGERSOURCE__MASTERPULSE);

        status = setFeature(DCAM_IDPROP_MASTERPULSE_INTERVAL, value);
        status = setDoubleParam(hMasterPulseInterval, value);

        status = setFeature(DCAM_IDPROP_MASTERPULSE_MODE,
                            DCAMPROP_MASTERPULSE_MODE__CONTINUOUS);
        status = setIntegerParam(hMasterPulseMode,
                                 DCAMPROP_MASTERPULSE_MODE__CONTINUOUS);
      }
    } else {
      if (value <= dvalue) status = asynError;
    }
  }

  else if (index < FIRST_HAMA_PARAM) {
    status = ADDriver::writeFloat64(pasynUser, value);
  }

  else {
    int prop = 0;
    if (index == hTriggerDelay) {
      prop = DCAM_IDPROP_TRIGGERDELAY;
    } else if (index == hOutputTriggerDelay0) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_DELAY;
    } else if (index == hOutputTriggerDelay1) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_DELAY + 0x100;
    } else if (index == hOutputTriggerDelay2) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_DELAY + 0x200;
    } else if (index == hOutputTriggerPeriod0) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_PERIOD;
    } else if (index == hOutputTriggerPeriod1) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_PERIOD + 0x100;
    } else if (index == hOutputTriggerPeriod2) {
      prop = DCAM_IDPROP_OUTPUTTRIGGER_PERIOD + 0x200;
    } else if (index == hMasterPulseInterval) {
      prop = DCAM_IDPROP_MASTERPULSE_INTERVAL;
    } else if (index == hInternalLineSpeed) {
      prop = DCAM_IDPROP_INTERNALLINESPEED;
    } else if (index == hInternalLineInterval) {
      prop = DCAM_IDPROP_INTERNAL_LINEINTERVAL;
    } else {
      return asynError;
    }

    m_err = dcamprop_setgetvalue(m_hdcam, prop, &dvalue);
    checkAndLogError(m_err, "dcamprop_setgetvalue()", prop, dvalue,
                     functionName);
  }

  if (status) {
    asynPrint(pasynUser, ASYN_TRACE_ERROR,
              "%s:%s: error, status=%d function=%d, paramName=%s, value=%f\n",
              driverName, functionName, status, index, paramName, value);

  } else {
    setDoubleParam(index, value);
    callParamCallbacks();
    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, paramName=%s, value=%f\n", driverName,
              functionName, index, paramName, value);
  }

  return status;
}

//============================================================================
void Orca::report(FILE* fp, int details) {
  double dvalue = 0;
  fprintf(fp, "Hammamatsu Orca Flash4.0 driver\n");
  if (details < 1) return;

  // Sensor mode and speed
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SENSORMODE, &dvalue);
  fprintf(fp, "SENSOR MODE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_READOUTSPEED, &dvalue);
  fprintf(fp, "READOUT SPEED:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_READOUT_DIRECTION, &dvalue);
  fprintf(fp, "READOUT DIRECTION:\t%f\n", dvalue);
  // Trigger
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGERSOURCE, &dvalue);
  fprintf(fp, "TRIGGER SOURCE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGER_MODE, &dvalue);
  fprintf(fp, "TRIGGER MODE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGERACTIVE, &dvalue);
  fprintf(fp, "TRIGGER ACTIVE:\t%f\n", dvalue);
  m_err =
      dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGER_GLOBALEXPOSURE, &dvalue);
  fprintf(fp, "TRIGGER GLOBAL EXPOSURE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGERPOLARITY, &dvalue);
  fprintf(fp, "TRIGGER POLARITY:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGER_CONNECTOR, &dvalue);
  fprintf(fp, "TRIGGER CONNECTOR:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGERTIMES, &dvalue);
  fprintf(fp, "TRIGGER TIMES:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TRIGGERDELAY, &dvalue);
  fprintf(fp, "TRIGGER DELAY:\t%f\n", dvalue);
  m_err =
      dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTERNALTRIGGER_HANDLING, &dvalue);
  fprintf(fp, "TRIGGER HANDLING:\t%f\n", dvalue);
  // Sensor cooler
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SENSORCOOLERSTATUS, &dvalue);
  fprintf(fp, "SENSOR COOLER STATUS:\t%f\n", dvalue);
  // Binning and ROI
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BINNING, &dvalue);
  fprintf(fp, "BINNING:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SUBARRAYHSIZE, &dvalue);
  fprintf(fp, "SUBARRAY HSIZE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SUBARRAYVSIZE, &dvalue);
  fprintf(fp, "SUBARRAY VSIZE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SUBARRAYHPOS, &dvalue);
  fprintf(fp, "SUBARRAY HPOS:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SUBARRAYVPOS, &dvalue);
  fprintf(fp, "SUBARRAY MODE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SUBARRAYMODE, &dvalue);
  fprintf(fp, "SUBARRAY MODE:\t%f\n", dvalue);
  // Feature
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_EXPOSURETIME, &dvalue);
  fprintf(fp, "EXPOSURE TIME:\t%f\n", dvalue);
  // ALU
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_DEFECTCORRECT_MODE, &dvalue);
  fprintf(fp, "DEFECT CORRETION MODE:\t%f\n", dvalue);
  m_err =
      dcamprop_getvalue(m_hdcam, DCAM_IDPROP_HOTPIXELCORRECT_LEVEL, &dvalue);
  fprintf(fp, "HOT PIXEL CORRECTION LEVEL:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTENSITYLUT_MODE, &dvalue);
  fprintf(fp, "INTENSITY LUT MODE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTENSITYLUT_PAGE, &dvalue);
  fprintf(fp, "INTENSITY LUT PAGE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_EXTRACTION_MODE, &dvalue);
  fprintf(fp, "EXTRACTION MODE:\t%f\n", dvalue);
  // Output Trigger
  m_err = dcamprop_getvalue(
      m_hdcam, DCAM_IDPROP_NUMBEROF_OUTPUTTRIGGERCONNECTOR, &dvalue);
  fprintf(fp, "OUTPUT TRIGGER CONNECTOR:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_OUTPUTTRIGGER_ACTIVE, &dvalue);
  fprintf(fp, "OUTPUT TRIGGER ACTIVE:\t%f\n", dvalue);

  // - master pulse
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_MASTERPULSE_MODE, &dvalue);
  fprintf(fp, "MASTER PULSE MODE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_MASTERPULSE_TRIGGERSOURCE,
                            &dvalue);
  fprintf(fp, "MASTER MODE TRIGGERSOURCE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_MASTERPULSE_INTERVAL, &dvalue);
  fprintf(fp, "MASTER MODE INTERVAL:\t%f\n", dvalue);
  m_err =
      dcamprop_getvalue(m_hdcam, DCAM_IDPROP_MASTERPULSE_BURSTTIMES, &dvalue);
  fprintf(fp, "MASTER MODE BURSTTIMES:\t%f\n", dvalue);

  // - synchronous timing
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_READOUTTIME, &dvalue);
  fprintf(fp, "TIMING READOUT TIME:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_CYCLICTRIGGERPERIOD,
                            &dvalue);
  fprintf(fp, "TIMING CYCLIN TRIGGER PERIOD:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_MINTRIGGERBLANKING,
                            &dvalue);
  fprintf(fp, "TIMING MIN TRIGGER BLANKING:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_MINTRIGGERINTERVAL,
                            &dvalue);
  fprintf(fp, "TIMING MIN TRIGGER INTERVAL:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_GLOBALEXPOSUREDELAY,
                            &dvalue);
  fprintf(fp, "TIMING GLOBAL EXPOSURE DELAY:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_EXPOSURE, &dvalue);
  fprintf(fp, "TIMING EXPOSURE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMING_INVALIDEXPOSUREPERIOD,
                            &dvalue);
  fprintf(fp, "TIMING INVALID EXPOSURE PERIOD:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTERNALFRAMERATE, &dvalue);
  fprintf(fp, "INTERNAL FRAME RATE:\t%f\n", dvalue);
  m_err =
      dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTERNAL_FRAMEINTERVAL, &dvalue);
  fprintf(fp, "INTERNAL FRAME INTERVAL:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTERNALLINESPEED, &dvalue);
  fprintf(fp, "INTERNAL LINE SPEED:\t%f\n", dvalue);
  m_err =
      dcamprop_getvalue(m_hdcam, DCAM_IDPROP_INTERNAL_LINEINTERVAL, &dvalue);
  fprintf(fp, "INTERNAL LINE INTERVAL:\t%f\n", dvalue);
  // - system information
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_COLORTYPE, &dvalue);
  fprintf(fp, "COLOR TYPE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BITSPERCHANNEL, &dvalue);
  fprintf(fp, "BITS PER CHANNEL:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_ROWBYTES, &dvalue);
  fprintf(fp, "IMAGE ROW BYTES:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_FRAMEBYTES, &dvalue);
  fprintf(fp, "IMAGE FRAME BYTES:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_TOPOFFSETBYTES, &dvalue);
  fprintf(fp, "IMAGE TOP OFFSET BYTES:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_PIXELTYPE, &dvalue);
  fprintf(fp, "IMAGE PIXEL TYPE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BUFFER_ROWBYTES, &dvalue);
  fprintf(fp, "BUFFER ROW BYTES:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BUFFER_FRAMEBYTES, &dvalue);
  fprintf(fp, "BUFER FRAME BYTES:\t%f\n", dvalue);
  m_err =
      dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BUFFER_TOPOFFSETBYTES, &dvalue);
  fprintf(fp, "BUFFER TOP OFFSET BYTES:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_BUFFER_PIXELTYPE, &dvalue);
  fprintf(fp, "BUFFER PIXEL TYPE:\t%f\n", dvalue);
  m_err =
      dcamprop_getvalue(m_hdcam, DCAM_IDPROP_RECORDFIXEDBYTES_PERFILE, &dvalue);
  fprintf(fp, "RECORD FIXED BYTES PER FILE:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_RECORDFIXEDBYTES_PERSESSION,
                            &dvalue);
  fprintf(fp, "RECORD FIXED BYTES PER SESSION:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_RECORDFIXEDBYTES_PERFRAME,
                            &dvalue);
  fprintf(fp, "RECORD FIXED BYTES PER FRAME:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_SYSTEM_ALIVE, &dvalue);
  fprintf(fp, "SYSTEM ALIVE:\t%f\n", dvalue);
  m_err =
      dcamprop_getvalue(m_hdcam, DCAM_IDPROP_CONVERSIONFACTOR_COEFF, &dvalue);
  fprintf(fp, "CONVERSION FACTOR COEFFICIENT:\t%f\n", dvalue);
  m_err =
      dcamprop_getvalue(m_hdcam, DCAM_IDPROP_CONVERSIONFACTOR_OFFSET, &dvalue);
  fprintf(fp, "CONVERSION FACTOR OFFSET:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_NUMBEROF_VIEW, &dvalue);
  fprintf(fp, "NUMBER OF VIEW:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_WIDTH, &dvalue);
  fprintf(fp, "IMAGE WIDTH:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_IMAGE_HEIGHT, &dvalue);
  fprintf(fp, "IMAGE HEIGHT:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DETECTOR_PIXEL_NUM_HORZ, &dvalue);
  fprintf(fp, "PIXEL NUM HORZ:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DETECTOR_PIXEL_NUM_VERT, &dvalue);
  fprintf(fp, "PIXEL NUM VERT:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_TIMESTAMP_PRODUCER, &dvalue);
  fprintf(fp, "TIMESTAMP PRODUCER:\t%f\n", dvalue);
  m_err = dcamprop_getvalue(m_hdcam, DCAM_IDPROP_FRAMESTAMP_PRODUCER, &dvalue);
  fprintf(fp, "FRAMESTAMP PRODUCER:\t%f\n", dvalue);

  ADDriver::report(fp, details);
}

int Orca::connectCamera(void) {
  static const char* functionName = "connectCamera";
  int nDevices = 0;
  int iDevice = 0;

  /* initialize api */
  memset(&m_apiInit, 0, sizeof(m_apiInit));
  m_apiInit.size = sizeof(m_apiInit);

  m_err = dcamapi_init(&m_apiInit);
  if (failed(m_err)) {
    ERR_ARGS("DCAMERR: 0x%08X [%s]", m_err, "dcamapi_init");
  } else {
    nDevices = m_apiInit.iDeviceCount;
    FLOW_ARGS("dcamapi_init() found %d device(s)", nDevices);
    iDevice = 0;
  }

  /* open handle to camera */
  memset(&m_devOpen, 0, sizeof(m_devOpen));
  m_devOpen.size = sizeof(m_devOpen);
  m_devOpen.index = iDevice;
  m_err = dcamdev_open(&m_devOpen);

  if (failed(m_err)) {
    ERR_ARGS("DCAMERR: 0x%08X [%s]", m_err, "dcamdev_open");
  } else {
    m_hdcam = m_devOpen.hdcam;
  }

  // open wait handle
  DCAMWAIT_OPEN waitopen;
  memset(&waitopen, 0, sizeof(waitopen));
  waitopen.size = sizeof(waitopen);
  waitopen.hdcam = m_hdcam;

  m_err = dcamwait_open(&waitopen);
  if (failed(m_err)) {
    ERR_ARGS("DCAMERR: 0x%08X [%s]", m_err, "dcamwait_open");
  } else {
    hwait = waitopen.hwait;
  }

  return 0;
}

int Orca::allocateBuffers(unsigned int nBuffers) {
  static const char* functionName = "allocateBuffers";
  m_err = dcambuf_alloc(m_hdcam, nBuffers);
  if (failed(m_err)) {
    ERR_ARGS("DCAMERR: 0x%08X [%s]", m_err, "dcambuf_alloc");
    return asynError;
  }
  return asynSuccess;
}

int Orca::freeBuffers() {
  static const char* functionName = "freeBuffers";
  m_err = dcambuf_release(m_hdcam);
  if (failed(m_err)) ERR_ARGS("DCAMERR: 0x%08X [%s]", m_err, "dcambuf_release");
  return 0;
}

asynStatus Orca::stopAcquire(void) {
  static const char* functionName = "stopAcquire";
  asynStatus status = asynSuccess;
  m_err = dcamcap_stop(m_hdcam);
  if (failed(m_err)) {
    ERR_ARGS("DCAMERR: 0x%08X [%s]", m_err, "dcamcap_stop");
    status = asynError;
  }

  freeBuffers();
  return status;
}

asynStatus Orca::startAcquire(void) {
  asynStatus status = asynSuccess;
  static const char* functionName = "startAcquire";

  // release buffer
  freeBuffers();

  // allocate buffer
  allocateBuffers(1);

  // start capture
  m_err = dcamcap_start(m_hdcam, DCAMCAP_START_SEQUENCE);
  if (failed(m_err)) {
    ERR_ARGS("DCAMERR: 0x%08X [%s]", m_err, "dcamcap_start");
    status = asynError;
  }
  return status;
}

inline void* memcpy_s(void* dst, size_t dstsize, const void* src,
                      size_t srclen) {
  if (dstsize < srclen)
    return memcpy(dst, src, dstsize);
  else
    return memcpy(dst, src, srclen);
}

asynStatus Orca::accessCapturedImage(HDCAM hdcam, int32 iFrame, void* buf,
                                     int32 rowbytes, int32 cx, int32 cy,
                                     epicsUInt32& ts_sec,
                                     epicsUInt32& ts_microsec) {
  const char* functionName = "accessCapturedImage";
  DCAMERR err;

  // prepare frame param
  DCAMBUF_FRAME bufframe;
  memset(&bufframe, 0, sizeof(bufframe));
  bufframe.size = sizeof(bufframe);
  bufframe.iFrame = iFrame;

  // access image
  err = dcambuf_lockframe(hdcam, &bufframe);
  if (failed(err)) {
    ERR_ARGS("DCAMERR: 0x%08X [%s]", m_err, "dcambuf_lockframe");
    return asynError;
  }

  if (bufframe.type != DCAM_PIXELTYPE_MONO16) {
    FLOW("not implement pixel type");
    return asynError;
  }

  // Get timestamp from the the frame
  ts_sec = bufframe.timestamp.sec;
  ts_microsec = bufframe.timestamp.microsec;

  // copy target ROI
  int32 copyrowbytes = cx * 2;
  char* pSrc = (char*)bufframe.buf;
  char* pDst = (char*)buf;

  for (int y = 0; y < cy; y++) {
    memcpy_s(pDst, rowbytes, pSrc, copyrowbytes);
    pSrc += bufframe.rowbytes;
    pDst += rowbytes;
  }

  return asynSuccess;
}

void Orca::getImageInformation(HDCAM hdcam, int32& pixeltype, int32& width,
                               int32& rowbytes, int32& height,
                               int32& framebytes) {
  double propertyValue;

  dcamprop_getvalue(hdcam, DCAM_IDPROP_IMAGE_PIXELTYPE, &propertyValue);
  pixeltype = (int32)propertyValue;
  dcamprop_getvalue(hdcam, DCAM_IDPROP_IMAGE_WIDTH, &propertyValue);
  width = (int32)propertyValue;
  dcamprop_getvalue(hdcam, DCAM_IDPROP_IMAGE_ROWBYTES, &propertyValue);
  rowbytes = (int32)propertyValue;
  dcamprop_getvalue(hdcam, DCAM_IDPROP_IMAGE_HEIGHT, &propertyValue);
  height = (int32)propertyValue;
  dcamprop_getvalue(hdcam, DCAM_IDPROP_IMAGE_FRAMEBYTES, &propertyValue);
  framebytes = (int32)propertyValue;
}

asynStatus Orca::imageTransferStatus(HDCAM hdcam,
                                     DCAMCAP_TRANSFERINFO& captransferinfo) {
  DCAMERR err;
  const char* functionName = "imageTransferStatus";
  memset(&captransferinfo, 0, sizeof(captransferinfo));
  captransferinfo.size = sizeof(captransferinfo);

  // get number of captured image
  err = dcamcap_transferinfo(m_hdcam, &captransferinfo);
  if (failed(err)) {
    ERR_ARGS("DCAMERR: 0x%08X [%s]", m_err, "dcamcap_transferinfo");
    return asynError;
  }

  if (captransferinfo.nFrameCount < 1) {
    return asynError;
  }

  return asynSuccess;
}

asynStatus Orca::setFeature(int featureIndex, double value) {
  DCAMERR err;
  const char* functionName = "setAPIFeature";
  err = dcamprop_setvalue(m_hdcam, featureIndex, value);
  if (failed(err)) {
    char text[256] = {'\0'};
    dcamprop_getname(m_hdcam, featureIndex, text, sizeof(text));
    ERR_ARGS("DCAMERR: 0x%08X [%s: %s]", m_err, "dcamprop_getvalue", text);
    return asynError;
  }
  return asynSuccess;
}

int Orca::roundToNearestMultipleOfFour(int value) {
  int remainder = value % 4;
  if (remainder == 0)
    return value;
  else if (remainder <= 2)
    return value - remainder;
  else
    return value + (4 - remainder);
}

bool Orca::checkAndLogError(DCAMERR err, const char* msg, int idx, double val,
                            const char* functionName) {
  bool isFailed = false;
  if (failed(err)) {
    char text[256];
    dcamprop_getname(m_hdcam, idx, text, sizeof(text));
    ERR_ARGS("Name: %s [0x%08x] - DCAMERR: 0x%08X", text, idx, err);
    isFailed = true;
  }
  return isFailed;
}

/* Code for iocsh registration */
extern "C" int OrcaConfig(const char* portName, int maxBuffers,
                          size_t maxMemory, int priority, int stackSize,
                          int maxFrames) {
  new Orca(portName, maxBuffers, maxMemory, priority, stackSize, maxFrames);

  return (asynSuccess);
}

static const iocshArg OrcaConfigArg0 = {"Port name", iocshArgString};
static const iocshArg OrcaConfigArg1 = {"maxBuffers", iocshArgInt};
static const iocshArg OrcaConfigArg2 = {"maxMemory", iocshArgInt};
static const iocshArg OrcaConfigArg3 = {"priority", iocshArgInt};
static const iocshArg OrcaConfigArg4 = {"stackSize", iocshArgInt};
static const iocshArg OrcaConfigArg5 = {"maxFrames", iocshArgInt};
static const iocshArg* const OrcaConfigArgs[] = {
    &OrcaConfigArg0, &OrcaConfigArg1, &OrcaConfigArg2,
    &OrcaConfigArg3, &OrcaConfigArg4, &OrcaConfigArg5};

static const iocshFuncDef configorca = {"devOrcaConfig", 6, OrcaConfigArgs};

static void configorcaCallFunc(const iocshArgBuf* args) {
  OrcaConfig(args[0].sval, args[1].ival, args[2].ival, args[3].ival,
             args[4].ival, args[5].ival);
}

static void orcaRegister(void) {
  iocshRegister(&configorca, configorcaCallFunc);
}

extern "C" {
epicsExportRegistrar(orcaRegister);
}
