ADHama
======
European Spallation Source ERIC Site-specific EPICS module : ADHama

Additonal information:

* [Documentation](https://confluence.esss.lu.se/display/IS/Integration+by+ICS)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/e3-recipes/ADHama-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)

To build conda env:

conda create -n hama epics-base require tclx make compilers adcore busy

To activate env:

conda activate orca

git clone ...

cd git/ADOrca

compilation:

make -f ADOrca.Makefile LIBVERSION=0.1 clean uninstall build db install

execution:

iocsh.bash cmds/st.cmd
